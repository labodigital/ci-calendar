<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('style_calendar'))
{
	function style_calendar($id=1)
	{
		$CI =& get_instance();
    	$CI->load->model('Ce_Templates');
    	$CI->Ce_Templates->load($id);

    	$field1 =	$CI->Ce_Templates->field1;
		$field2 =	$CI->Ce_Templates->field2;
		$field3 =	$CI->Ce_Templates->field3;
		$field4 =	$CI->Ce_Templates->field4;
		$field5 =	$CI->Ce_Templates->field5;
		$field6 =	$CI->Ce_Templates->field6;
		$field7 =	$CI->Ce_Templates->field7;
		$field8 =	$CI->Ce_Templates->field8;
		$field9 =	$CI->Ce_Templates->field9;
		$field10 =	$CI->Ce_Templates->field10;
		$field11 =	$CI->Ce_Templates->field11;
		$field12 =	$CI->Ce_Templates->field12;
		$field13 =	$CI->Ce_Templates->field13;
		$field14 =	$CI->Ce_Templates->field14;
		$field15 =	$CI->Ce_Templates->field15;
		$num_cal =  $CI->Ce_Templates->num_cal;

		$vel1= '';
		$vel2= '';
		if($field15 > 0)
		{
			$vel2="width:".$field15."%";
			$vel1="width:".($vel2-1)."%";
		}
        
        return "
				.calendar
				{
					float:left;
					$vel2
				}
				.box
				{
					float:left;position:relative;padding:2px; overflow:hidden;border:1px solid #f1f1f1;
					background:$field1;color:$field3;
					display:block;
					{$vel1}
				}

				.calendar-month
				{
					background:$field2;
					text-align:center;
				}
				.daysi
				{
					background:$field5;padding:$field11;color:$field7;text-align:center;font-size:".$field12."px;
				}
				.daysi1
				{
					background:$field9;padding:$field11;color:$field8;text-align:center;font-size:".$field12."px;
				}
				.daysi2
				{
					background:$field6;padding:$field11;color:$field8;text-align:center;font-size:".$field12."px;
				}
				.day_name
				{
					font-size:12px;padding:3px;background:$field10;color:$field4;
				}
				.calendar_footer
				{
					width:100%;
					font-size:11px;clear:both;background:$field2;
					float:left;
					padding:2px;
				}
				.calendar_down
				{
					padding:0px;
					margin:0px;
					width:80%;
					font-size:11px;
					float:left;
					padding-top:4px;
				}
				.calendar_down li
				{
					float:left;
					list-style:none;
				}
				.available
				{
					background:$field6;
				}
				.navailable
				{
					background:$field5
				}
				.pending
				{
					background:$field9
				}
				.reservation_button
				{
					border:0px;font-weight:bold;font-size:12px; padding:3px; background:$field13; color:$field14;cursor:pointer;
					float:right;
				}
				.disabled_calendar
				{
					width:100%;text-align:center;font-size:15px;
				}
				.arrow_left_right
				{
					position:relative;
				}
				.arrow_left_right a
				{

				}
				.arrow_right
				{
					width:15px;
					height:15px;
					position:absolute;top:2px;right:3px;
					border:0px;
				}
				.arrow_left
				{
					width:15px;
					height:15px;
					position:absolute;top:2px;left:3px;
					border:0px;
				}
				@media only screen
					and (min-device-width : 220px)
					and (max-device-width : 480px) {
					.box{width:99%;}
				  .calendar{width:100%;}
				.reservation_button {width:100%;padding:3px 0px;float:left;margin-top:6px;}
				  
				  }
				";

	}
}

if ( ! function_exists('calendar'))
{

	function calendar($year, $months, $days = array(), $day_name_length = 3, $month_href = NULL, $first_day = 0, $pn = array(),$id)
	{ 
		$CI =& get_instance();
    	$CI->load->model('Ce_Reserved_Days');
		$name_mes=array(" ","January","February","March","April","May","Jun","Jul","August","September","Oktober","November","December");
		$name_day=array("Sunday",	"Monday",	"Tuesday",	"Wednesday",	"Thursday",	"Friday",	"Saturday"); 
		global $val2;
		
		$first_of_month = gmmktime(0,0,0,$months,1,$year);
		$day_names = array();

		for($n=0,$t=(3+$first_day)*86400; $n<7; $n++,$t+=86400) 
		{
			$day_names[$n] = ucfirst(gmstrftime('%A',$t)); 
		}

		list($month, $year, $month_name, $weekday) = explode(',',gmstrftime('%m,%Y,%B,%w',$first_of_month));
		$weekday = ($weekday + 7 - $first_day - 1) % 7; 
		$puta=$month*1;
		$title   = "<b>".htmlentities(ucfirst($name_mes[$puta]), ENT_QUOTES, "UTF-8").'&nbsp;'.$year."</b>";  
		$calendar = '<table class="calendar" cellpadding="0" cellspacing="1"'.$val2.'>'."\n".
					'<caption class="calendar-month">'.($title)."</caption>\n<tr>";

		if($day_name_length)
		{
			foreach($day_names as $d =>$ke)
			{
 				if(strlen(htmlentities($name_day[$d], ENT_QUOTES, "UTF-8"))!=strlen(htmlentities($name_day[$d])))
 				{
 					$dod=4;
 				}
 				else
 				{
 					$dod=3;
 				}
					$calendar .= '<th class="day_name" abbr="'.htmlentities($d).'">'.htmlentities($day_name_length < 4 ? substr($name_day[$d],0,$dod) : $name_day[$d], ENT_QUOTES, "UTF-8").'</th>';
				}
			$calendar .= "</tr>\n<tr>";
		}

		$weekday=$weekday+1;
		if($weekday > 0) 
		{
			$calendar .= '<td colspan="'.$weekday.'">&nbsp;</td>'; #initial 'empty' days
		}
	
		for($day=1,$days_in_month=gmdate('t',$first_of_month); $day<=$days_in_month; $day++,$weekday++)
		{

			if($weekday == 7)
			{
				$weekday   = 0; 
				$calendar .= "</tr>\n<tr>";
			}

			if(isset($days[$day]) and is_array($days[$day]))
			{
				@list($link, $classes, $content) = $days[$day];
				if(is_null($content))  
				{
					$content  = $day;
				}

				$calendar .= '<td'.($classes ? ' class="'.htmlspecialchars($celasses).'">' : '>').
								($link ? '<a href="'.htmlspecialchars($link).'">'.$content.'</a>' : $content).'</td>';
			}
			else 
			{
   				$queryRd= $CI->Ce_Reserved_Days->getReservedDate($year,$month,$day,$id);
   				$sa1= $queryRd->num_rows();
   				$sas1 = $queryRd->row_array();

   				// estatus
 				if($sa1==1 and $sas1['status']==0) 
 				{
 					$ih="";  //disponible
 				}
 				else
 				{
 					if($sa1==1 and $sas1['status']==1)
 					{
 						$ih=1;
 					}	  
 					else 
 					{
 						$ih=2;
 					}
 				}
				$calendar .= "<td class='daysi$ih'>$day</td>";
  			}
		}

		if($weekday != 7)
		{ 
			$calendar .= '<td colspan="'.(7-$weekday).'">&nbsp;</td>'; 
		}

		return $calendar."</tr>\n</table>\n";

	}

}