<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('getDatesFromRange'))
{
	function getDatesFromRange($startDate, $endDate)
	{
    	$return = array($startDate);
    	$start = $startDate;
    	$i=1;
    	if (strtotime($startDate) < strtotime($endDate))
    	{
       		while (strtotime($start) < strtotime($endDate))
        	{
            	$start = date('Y-m-d', strtotime($startDate.'+'.$i.' days'));
            	$return[] = $start;
            	$i++;
        	}
    	}

    	return $return;
	}
}