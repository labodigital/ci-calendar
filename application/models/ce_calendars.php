<?php


require_once  "ce_calendars_dao.php";
class Ce_Calendars extends Ce_Calendars_Dao
{
	const DB_TABLE = 'ce_calendars';
	const DB_TABLE_PK = 'id';

	public $id;
    public $ime;
    public $kod;
    public $active;
	public $id_page;
	public $template;
	
	
	public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? trim($data['id']) : 0;
        $this->ime                      = (isset($data['ime'])) ? trim($data['ime']) : null;
        $this->kod                      = (isset($data['kod'])) ? trim($data['kod']) : null;
        $this->active                   = (isset($data['active'])) ? trim($data['active']) : null;
        $this->id_page               	= (isset($data['id_page'])) ? trim($data['id_page']) : null;
        $this->template       		    = (isset($data['template'])) ? trim($data['template']) : null;
        
    }

   
}

