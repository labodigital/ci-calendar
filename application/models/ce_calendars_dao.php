<?php


 require_once  "abstractclass_basic_model.php";
 class Ce_Calendars_Dao extends Abstractclass_basic_model 
 {
 	
	public function getActiveCalendarsByID($id)
 	{
 		$this->db->select("*");
   		$this->db->from($this::DB_TABLE);
    	$this->db->where('id', $id);
    	$this->db->where('active', 'Y');
   		$query = $this->db->get(); 
   		return $query;
 	} 	 


 }
