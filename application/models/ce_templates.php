<?php


require_once  "ce_templates_dao.php";
class Ce_Templates extends Ce_Templates_Dao
{
	const DB_TABLE = 'ce_templates';
	const DB_TABLE_PK = 'id';

	public $id;
    public $num_cal;
    public $field1;
    public $field2;
	public $field3;
	public $field4;
	public $field5;
	public $field6;
	public $field7;
    public $field8;
	public $field9;
	public $field10;
	public $field11;
	public $field12;
	public $field13;
	public $field14;
	public $field15;
	
	public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? trim($data['id']) : 0;
        $this->num_cal                  = (isset($data['num_cal'])) ? trim($data['num_cal']) : null;
        $this->field1                   = (isset($data['field1'])) ? trim($data['field1']) : null;
        $this->field2                   = (isset($data['field2'])) ? trim($data['field2']) : null;
        $this->field3               	= (isset($data['field3'])) ? trim($data['field3']) : null;
        $this->field4       		    = (isset($data['field4'])) ? trim($data['field4']) : null;
        $this->field5      				= (isset($data['field5'])) ? trim($data['field5']) : null;
        $this->field6          	        = (isset($data['field6'])) ? trim($data['field6']) : null;
        $this->field7           		= (isset($data['field7'])) ? trim($data['field7']) : null;
        $this->field8                   = (isset($data['field8'])) ? trim($data['field8']) : null;
        $this->field9             		= (isset($data['field9'])) ? trim($data['field9']) : null;
        $this->field10         			= (isset($data['field10'])) ? trim($data['field10']) : null;
        $this->field11                 	= (isset($data['field11'])) ? trim($data['field11']) : null;
        $this->field12                 	= (isset($data['field12'])) ? trim($data['field12']) : null;
        $this->field13                  = (isset($data['field13'])) ? trim($data['field13']) : null;
        $this->field14                  = (isset($data['field14'])) ? trim($data['field14']) : null;
        $this->field15                  = (isset($data['field15'])) ? trim($data['field15']) : null;
        
    }

   
}

