<?php


require_once  "ce_settings_dao.php";
class Ce_Settings extends Ce_Settings_Dao
{
	const DB_TABLE = 'ce_settings';
	const DB_TABLE_PK = 'id';

	public $id;
    public $fields;
    public $vrednosti;
	
	
	public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? trim($data['id']) : 0;
        $this->fields                   = (isset($data['fields'])) ? trim($data['fields']) : null;
        $this->vrednosti                = (isset($data['vrednosti'])) ? trim($data['vrednosti']) : null;
    }

   
}

