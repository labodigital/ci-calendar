<?php


 require_once  "abstractclass_basic_model.php";
 class Listings_Dao extends Abstractclass_basic_model 
 {
 	
 	 public function getAllListingsByUserID($userID)
 	  {
 	  		$query = $this->db->query("
 	  				SELECT list.*,(SELECT CONCAT_WS(\"/\",Path,Name)  FROM ListingsMedia where ID=list.ProfilePhoto) as src 
 	  				FROM Listings as list 
 	  				WHERE UserID='{$userID}' AND Status='Active'

 	  				");      
   			return $query;	
 	  }
 	  		

 	  public function getAllListingsByType($type)
 	  {
 	  		$query = $this->db->query("
 	  				SELECT list.*,(SELECT CONCAT_WS('/',`Path`,`Name`)  FROM ListingsMedia where ID=list.ProfilePhoto) as src 
 	  				FROM Listings as list 
 	  				WHERE Type='{$type}' AND Status='Active'

 	  				"); 
   			return $query;
 	  }

 	  public function getPaginationListingByType($limit,$start,$type)
 	  {
 	  		$query = $this->db->query("
 	  				SELECT list.*,(SELECT CONCAT_WS(\", \",first_name,last_name) FROM users where id=list.UserID) as usernameformat,
 	  				(SELECT CONCAT_WS(\"/\",`Path`,`Name`)  FROM ListingsMedia where ID=list.ProfilePhoto) as src 
 	  				FROM Listings as list WHERE list.Type ='{$type}' AND Status='Active' LIMIT {$start}, {$limit}");      
   			return $query;	
 	  }


 	  public function getListingByID($listingID)
 	  {
 	  		$query = $this->db->query("
 	  				SELECT list.*,(SELECT CONCAT_WS(\", \",first_name,last_name) FROM users where id=list.UserID) as usernameformat
 	  				FROM Listings as list 
 	  				WHERE list.ID='{$listingID}' AND Status='Active'
 	  				"); 
   			return $query;
 	  }


 	  public function getSearchListings($sqlWhere = FALSE, $datecheck = FALSE)
 	  {
 	  	$whereoptions = "";
 	  	$dateSql = "";
 	  	if($sqlWhere)
 	  	{
 	  		$whereoptions = "WHERE {$sqlWhere} AND list.Status='Active'";
 	  	}

 	  	if($datecheck)
 	  	{
 	  		$dateSql = "JOIN ListingAvailabilityTimeFrame as av ON list.ID = av.ListingID ";
 	  	}

 	  	$query = $this->db->query("
 	  				 	SELECT DISTINCT list.* FROM Listings as list 
						LEFT JOIN ListingSubtypes as sub ON list.ID= sub.ListingID
						LEFT JOIN ListingConditions as con ON list.ID = con.ListingID 
						{$dateSql} 
						{$whereoptions}

 	  				"); 
   			return $query;
 	  }


 	  public function getSearchListingPagination($limit,$start,$sqlWhere = FALSE, $datecheck = FALSE)
 	  {
 	  	$whereoptions = "";
 	  	$dateSql = "";
 	  	if($sqlWhere)
 	  	{
 	  		$whereoptions = "WHERE {$sqlWhere}";
 	  	}

 	  	if($datecheck)
 	  	{
 	  		$dateSql = "JOIN ListingAvailabilityTimeFrame as av ON list.ID = av.ListingID ";
 	  	}
 	  		$query = $this->db->query("
 	  				 	SELECT DISTINCT list.*,(SELECT CONCAT_WS(\"/\",`Path`,`Name`)  FROM ListingsMedia where ID=list.ProfilePhoto) as src,
 	  				 	(SELECT CONCAT_WS(\", \",first_name,last_name) FROM users where id=list.UserID) as usernameformat  
 	  				 	FROM Listings as list 
						LEFT JOIN ListingSubtypes as sub ON list.ID= sub.ListingID
						LEFT JOIN ListingConditions as con ON list.ID = con.ListingID 
						{$dateSql} 
						{$whereoptions} AND list.Status='Active'
						LIMIT {$start}, {$limit}

 	  				"); 
   			return $query;
 	  }


 }
