<?php


require_once  "ce_reserved_days_dao.php";
class Ce_Reserved_Days extends Ce_Reserved_Days_Dao
{
	const DB_TABLE = 'ce_reserved_days';
	const DB_TABLE_PK = 'id';

	public $id;
    public $godina;
    public $mesec;
    public $dan;
	public $date;
	public $id_calendar;
    public $id_reservation;
    public $status;
    public $from_to;
	
	
	public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? trim($data['id']) : 0;
        $this->godina                   = (isset($data['godina'])) ? trim($data['godina']) : null;
        $this->mesec                    = (isset($data['mesec'])) ? trim($data['mesec']) : null;
        $this->dan                      = (isset($data['dan'])) ? trim($data['dan']) : null;
        $this->date               	    = (isset($data['date'])) ? trim($data['date']) : null;
        $this->id_calendar       		= (isset($data['id_calendar'])) ? trim($data['id_calendar']) : null;
        $this->id_reservation           = (isset($data['id_reservation'])) ? trim($data['id_reservation']) : null;
        $this->status                   = (isset($data['status'])) ? trim($data['status']) : null;
        $this->from_to                  = (isset($data['from_to'])) ? trim($data['from_to']) : null;
        
    }

   
}

