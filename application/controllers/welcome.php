<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	private $data;

	public function __construct()
	{
		 parent::__construct();
		$this->load->helper('calendar_functions');
		//$this->load->helper('language');
	}


	/**
	 * Index Page for this controller. please make sure to merge
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}


	public function demo()
	{
		$this->data['domain']="http://dev.ci-calendar";
	//	$this->data['patH']="$domain/calendare";
		/* MySQL settings */
		$hostname_conn = "localhost";
		$database_conn = "calendar";
		$username_conn = "root";
		$password_conn = "hunter";
		$this->load->library('Mobile_Detect');
		$objMobile= new Mobile_Detect();
		$this->data['detect'] = $objMobile;
		$this->data['pathcalendar'] = base_url() . 'welcome';
		$this->load->view('demo_calendar',$this->data);
	}


	public function calend_read_main($id= false, $mes= false,$year = false,$random = false)
	{
		//	id="+id+"&mes=&god=&random=" + Math.random()
		//include("Connections/conn.php");

		if($year == false || $year == 'false'){
			$year = date('Y');
		}
		if($mes == false || $mes == 'false'){
			$mes = date('m');
		}
		if($id == false){
			//die('no id');
			$id=5;
		}

		$this->load->model('Ce_Calendars');
    	$queryCal = $this->Ce_Calendars->getActiveCalendarsByID($id);

		if($queryCal->num_rows()==0)
		{
			return;
		}

		$this->load->model('Ce_Templates');
		$this->Ce_Templates->load(1);

		$this->data['get_year'] = $year;
		$this->data['get_month'] = $mes;
		$this->data['get_calend'] = $id;
		$this->data['num_cal'] = 2;   // $this->Ce_Templates->num_cal;
		$this->data['patH'] = base_url() . 'welcome';
		$this->load->view('calend_read_main',$this->data);
	}

	private function getDbSettings()
	{
		$this->load->model('Ce_Settings');
		$querySet = $this->Ce_Settings->getAllSettings();
		$settings = array();
		
		foreach($querySet->result_array() as $row)
		{
			$settings[$row['fields']] = $row['vrednosti']; 
		}
		return $settings;
	}


	private function is_post()
	{
		$tempArray = $this->input->post(NULL,TRUE);
		if(empty($tempArray))
		{
			$this->postArray = FALSE;
			return FALSE;
		}
		$this->postArray = $tempArray;
		return TRUE;
	}


	public function reservation($kid=false,$random=false)
	{
   // href        : "<?php echo $patH/reservation_form.php?kid="+id+"&random="+ Math.random()});
		if($this->is_post())
		{
			$this->load->model('Ce_Reserved_Days');
			$klasa="radius_red";
    		//extract($_POST);
    		$m=0;
    		$ima_nema = 0;
    		$arrayDates= getDatesFromRange($this->postArray['odd'],$this->postArray['doo']);

    		foreach($arrayDates as $key => $value)
    		{
      			$m++;
    		}

    		foreach($arrayDates as $key => $value)
    		{
    			list($year,$month,$day) = explode('-',$value);
    			$queryRd = $this->Ce_Reserved_Days->getReservedDate($year,$month,$day,$kid);
        		$ima_nema +=$queryRd->num_rows();
    		}
    		
    		if($ima_nema>0)
    		{        
        		$msg="Some days, for this calendar, are already taken. Please select another date!";
        		print_r($msg);
    		}    
    		




		}
		else
		{


			$this->data['msg'] = '';
		}



		$this->data['settings'] = $this->getDbSettings(); 

		$this->load->view('reservation_form',$this->data);
	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */